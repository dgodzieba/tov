function EOS_MR_Compute(seed,n)
%% Mass-Radius Curve Calculations
%--------------------------------------------------------------------------

% This script computes the mass-radius curve (with a specified number of
% points along the curve) for each EOS in the file EOS_set_seed.mat.

%--------------------------------------------------------------------------
ws = load('EOS_set_' + string(seed) + '.mat');
G = ws.G;
lgrho = ws.lgrho;
rhom = ws.rhom;

rtov = [1e-6:(20-1e-6)/2000:20];
ptovmin = 0;
R = zeros(10000,n); % 2D array, stores radii.
M = zeros(10000,n); % 2D array, stores masses.

tic
parfor i=1:10000
    %----------------------------------------------------------------------
    eos = Create_EOS(G(i,:),lgrho(i,:));
    %----------------------------------------------------------------------
    % For each EOS, the TOV eq. will be solved for n2 density values from
    % rho = 5e-4 to rhom.
    rho = linspace(5e-4,rhom(i),n);
    
    rtov = [1e-6:(20-1e-6)/2000:20];
    ptovmin = 0;
    for j=1:n
        % Solve TOV eq. for each density value, skipping over any errors.
        try
            tov  = TOVL(rho(j),eos,1,rtov,ptovmin,0);
        catch
            continue
        end
        % Skip over EOS that fail to reach star radius.
        if ~size(tov,1)
            continue
        end
        % Store NS mass and radius for each density value.
        M(i,j) = tov.M;
        R(i,j) = tov.R;
    end
end
toc
disp('Mass-Radius Computation Complete')


% Save arrays with timestamp.
filename = 'MR_' + string(seed) + '.mat';
save(filename,'M','R');



end
