function EOS_Lambda_Compute(num_EOS,num_dens,seed)
%% Tidal Deformability Calculations
%--------------------------------------------------------------------------

% This script computes the lth order electric tidal Love number k_l, the
% corresponding tidal deformability \Lambda_l, and the compactness C for
% a specified number of NS solutions for a specified number of EOSs from
% the file EOS_set_seed.mat.

%--------------------------------------------------------------------------
ws = load('EOS_set_' + string(seed) + '.mat');

G     = ws.G;
lgrho = ws.lgrho;
lam2  = ws.lam2;
rhom  = ws.rhom;

n       = num_EOS;  % Number of of EOSs we want to look at.
n2      = num_dens; % Number of density values for which to solve per EOS.
C       = zeros(n,n2); % 2D array, stores the compactness of each EOS
Love2   = zeros(n,n2); % 2D array, stores k2.
Love3   = zeros(n,n2); % 2D array, stores k3.
Love4   = zeros(n,n2); % 2D array, stores k4.
%Love5   = zeros(n,n2); % 2D array, stores k5.
%Love6   = zeros(n,n2); % 2D array, stores k6.
%Love7   = zeros(n,n2); % 2D array, stores k7.
%Love8   = zeros(n,n2); % 2D array, stores k8.
lambda2 = zeros(1,n*n2); % Stores lambda2.
lambda3 = zeros(1,n*n2); % Stores lambda3.
lambda4 = zeros(1,n*n2); % Stores lambda4.
%lambda5 = zeros(1,n*n2); % Stores lambda5.
%lambda6 = zeros(1,n*n2); % Stores lambda6.
%lambda7 = zeros(1,n*n2); % Stores lambda7.
%lambda8 = zeros(1,n*n2); % Stores lambda8.

%--------------------------------------------------------------------------

% The love numbers for each EOS will be computed for n2 density values from
% rho = 5e-4 to rhom.

%drho = (5e-3 - 5e-4)/(n2-1);

tic
rtov = [1e-6:(20-1e-6)/3000:20];
ptovmin = 0;
parfor i=1:n
    if isnan(lam2(i))
        continue
    end
    eos = Create_EOS(G(i,:),lgrho(i,:));
    for j=1:n2
        % Solve TOV eq. for each density value, skipping over any errors.
        try
            tov  = TOVL(5e-4+(rhom(i)-5e-4)/(n2-1)*(j-1),eos,[2,3,4],rtov,ptovmin,0);
            %tov  = TOVL(5e-4+(rhom(i)-5e-4)/(n2-1)*(j-1),eos,[2,5,6],rtov,ptovmin,0);
            %tov  = TOVL(5e-4+(rhom(i)-5e-4)/(n2-1)*(j-1),eos,[2,7,8],rtov,ptovmin,0);
        catch
            continue
        end
        % Skip over EOS that fail to reach star radius.
        if size(tov,1)==0
            continue
        end
        % Store Love numbers for each density value.
        Love2(i,j) = tov.kl(2);
        Love3(i,j) = tov.kl(3);
        Love4(i,j) = tov.kl(4);
        %Love5(i,j) = tov.kl(5);
        %Love6(i,j) = tov.kl(6);
        %Love7(i,j) = tov.kl(7);
        %Love8(i,j) = tov.kl(8);
        % Store compactness for each density value.
        C(i,j) = tov.C;
    end
end
Love2 = reshape(Love2,[1 n*n2]);
Love3 = reshape(Love3,[1 n*n2]);
Love4 = reshape(Love4,[1 n*n2]);
%Love5 = reshape(Love5,[1 n*n2]);
%Love6 = reshape(Love6,[1 n*n2]);
%Love7 = reshape(Love7,[1 n*n2]);
%Love8 = reshape(Love8,[1 n*n2]);
C     = reshape(C,[1 n*n2]);

%--------------------------------------------------------------------------

parfor i=1:n*n2
    % Compute lambdas from Love numbers and compactness.
    try
        lambda2(i) = Love2(i)*2/(3) / C(i)^5;
        lambda3(i) = Love3(i)*2/(3*5) / C(i)^7;
        lambda4(i) = Love4(i)*2/(3*5*7) / C(i)^9;
        %lambda5(i) = Love5(i)*2/(3*5*7*9) / C(i)^11;
        %lambda6(i) = Love6(i)*2/(3*5*7*9*11) / C(i)^13;
        %lambda7(i) = Love7(i)*2/(3*5*7*9*11*13) / C(i)^15;
        %lambda8(i) = Love8(i)*2/(3*5*7*9*11*13*15) / C(i)^17;
    catch
        continue
    end
end

%--------------------------------------------------------------------------

disp('Lambda Computation Complete')
toc

%--------------------------------------------------------------------------

% Save arrays with timestamp.
filename = 'Lambda234_set_' + string(seed) + '.mat';
save(filename,'n2','C','Love2','Love3','Love4','lambda2','lambda3','lambda4');
%filename = 'Lambda256_set_' + string(seed) + '.mat';
%save(filename,'n2','C','Love2','Love5','Love6','lambda2','lambda5','lambda6');
%filename = 'Lambda278_set_' + string(seed) + '.mat';
%save(filename,'n2','C','Love2','Love7','Love8','lambda2','lambda7','lambda8');

%--------------------------------------------------------------------------

end


