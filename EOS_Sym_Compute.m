function EOS_Sym_Compute(num_EOS,num_sample,num_mass,seed)
%% Tidal Deformability Calculations
%--------------------------------------------------------------------------

% This script computes the symmetric and antisymmetrci combinations of
% \Lambda_2 for a specified number of random NS binaries for a random set 
% of EOS in the file EOS_set_seed.mat.

%--------------------------------------------------------------------------

ws = load('A_' + string(seed) + '.mat');

n       = num_mass; % Number of NS binaries.
G       = ws.G;
lgrho   = ws.lgrho;
Mmax    = ws.Mmax;

% Select a set of random EOS.
rng(seed);
perm    = randperm(num_EOS);
sample  = perm(1:num_sample);
G       = G(sample,:);
lgrho   = lgrho(sample,:);
Mmax    = Mmax(sample);

Love1   = zeros(num_sample,n); % 2D array, stores k2 of the primary.
Love2   = zeros(num_sample,n); % 2D array, stores k2 of the secondary.
M1      = zeros(num_sample,n); % 2D array, stores mass of the primary.
M2      = zeros(num_sample,n); % 2D array, stores mass of the secondary.
C1      = zeros(num_sample,n); % 2D array, stores compactness of the primary.
C2      = zeros(num_sample,n); % 2D array, stores compactness of the secondary.

lambda1 = zeros(1,n*2500); % 2D array, stores \Lambda_2 of the primary.
lambda2 = zeros(1,n*2500); % 2D array, stores \Lambda_2 of the secondary.

%--------------------------------------------------------------------------

tic
parfor i=1:2500
    % Create an EOS with the valid parameters.
    eos = Create_EOS(G(i,:),lgrho(i,:));
    %----------------------------------------------------------------------
    % Create a random set of n binaries. M1 > M2.
    m1 = (Mmax(i)-1)*rand(1,n) + 1;
    m2 = (Mmax(i)-1)*rand(1,n) + 1;
    for j=1:n
       if m2(j) > m1(j)
           [m2(j),m1(j)] = deal(m1(j),m2(j));
       end
    end
    M1(i,:) = m1;
    M2(i,:) = m2;
    %----------------------------------------------------------------------
    L1 = zeros(1,n);
    L2 = zeros(1,n);
    c1 = zeros(1,n);
    c2 = zeros(1,n);
    for j=1:n
        % Solve TOV eq. for each density value, skipping over any errors.
        try
            tov1 = TOVIterate(eos.rhoc,eos,2,'M',m1(j),1.0e-9,200);
            tov2 = TOVIterate(eos.rhoc,eos,2,'M',m2(j),1.0e-9,200);
            % Skip over EOS that fail to reach star radius.
            if size(tov1,1)==0 || size(tov2,1)==0
                continue
            end
            if any(isnan(tov1.kl)) || any(isnan(tov2.kl))
                continue
            end
        catch
            continue
        end
        %------------------------------------------------------------------
        % Store Love numbers for each density value.
        L1(j) = tov1.kl(2);
        L2(j) = tov2.kl(2);
        % Store compactness for each density value.
        c1(j) = tov1.C;
        c2(j) = tov2.C;
    end
    Love1(i,:) = L1;
    Love2(i,:) = L2;
    C1(i,:) = c1;
    C2(i,:) = c2;
end
Love1  = reshape(Love1,[1 n*2500]);
Love2  = reshape(Love2,[1 n*2500]);
M1     = reshape(M1,[1 n*2500]);
M2     = reshape(M2,[1 n*2500]);
C1     = reshape(C1,[1 n*2500]);
C2     = reshape(C2,[1 n*2500]);

%--------------------------------------------------------------------------

parfor i=1:n*2500
    % Compute lambdas from Love numbers and compactness.
    lambda1(i) = Love1(i)*2/3 / C1(i)^5;
    lambda2(i) = Love2(i)*2/3 / C2(i)^5;
end

%--------------------------------------------------------------------------

% Compute symmetric and antisymmetric combinations.
lambdaS = (lambda1 + lambda2) / 2;
lambdaA = (lambda2 - lambda1) / 2;

disp('Symmetric Computation Complete')
toc

%--------------------------------------------------------------------------

% Save arrays with timestamp.
filename = 'Sym_set_' + string(seed) + '.mat';
save(filename,'n','C1','C2','lambdaS','lambdaA','M1','M2');

%--------------------------------------------------------------------------

end