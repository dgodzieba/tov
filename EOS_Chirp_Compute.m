function EOS_Chirp_Compute(num_EOS,num_sample,num_mass,seed)
%% Tidal Deformability Calculations
%--------------------------------------------------------------------------

% This script computes a specified number of random NS binaries with a
% chirp mass Mchirp for a random sample of EOS in the file EOS_set_seed.m.

%--------------------------------------------------------------------------

ws = load('A_' + string(seed) + '.mat');

n       = num_mass; % Number of NS binaries.

Mchirp  = 1.188; %Msun for GW170817.

G       = ws.G;
lgrho   = ws.lgrho;
lam2    = ws.lam2;

% Select a random set of EOS.
rng(seed);
perm    = randperm(num_EOS);
sample  = perm(1:num_sample);
G       = G(sample,:);
lgrho   = lgrho(sample,:);

lambda1   = zeros(num_sample,n); % 2D array, stores \Lambda_2 of the primary.
lambda2   = zeros(num_sample,n); % 2D array, stores \Lambda_2 of the secondary.
M1      = zeros(num_sample,n); % 2D array, stores mass of the primary.
M2      = zeros(num_sample,n); % 2D array, stores mass of the secondary.
C1      = zeros(num_sample,n); % 2D array, stores compactness of the primary.
C2      = zeros(num_sample,n); % 2D array, stores compactness of the secondary.

%--------------------------------------------------------------------------

tic
parfor i=1:2500
    % Create an EOS with the valid parameters.
    eos = Create_EOS(G(i,:),lgrho(i,:));
    %----------------------------------------------------------------------
    % Create a random set of n binaries of constant chirp mass from GW170817. M1 > M2.
    q  = (1-1/2)*rand(1,n) + 1/2;
    
    m1 = Mchirp.*(1+q).^(1/5)./q.^(3/5);
    m2 = m1.*q;
    
    M1(i,:) = m1;
    M2(i,:) = m2;
    %----------------------------------------------------------------------
    L1 = zeros(1,n);
    L2 = zeros(1,n);
    c1 = zeros(1,n);
    c2 = zeros(1,n);
    for j=1:n
        % Solve TOV eq. for each density value, skipping over any errors.
        try
            tov1 = TOVIterate(eos.rhoc,eos,2,'M',m1(j),1.0e-9,200);
            tov2 = TOVIterate(eos.rhoc,eos,2,'M',m2(j),1.0e-9,200);
        catch
            continue
        end
        % Skip over EOS that fail to reach star radius.
        if size(tov1,1)==0 || size(tov2,1)==0
            continue
        end
        lam1 = tov1.kl(2)*2/3 / tov1.C^5;
        lam2 = tov2.kl(2)*2/3 / tov2.C^5;
        if isnan(lam1) || isnan(lam2)
            continue
        end
        %------------------------------------------------------------------
        % Store Love numbers for each density value.
        L1(j) = lam1;
        L2(j) = lam2;
        % Store compactness for each density value.
        c1(j) = tov1.C;
        c2(j) = tov2.C;
    end
    lambda1(i,:) = L1;
    lambda2(i,:) = L2;
    C1(i,:) = c1;
    C2(i,:) = c2;
end
lambda1  = reshape(lambda1,[1 n*2500]);
lambda2  = reshape(lambda2,[1 n*2500]);
M1     = reshape(M1,[1 n*2500]);
M2     = reshape(M2,[1 n*2500]);
C1     = reshape(C1,[1 n*2500]);
C2     = reshape(C2,[1 n*2500]);

%--------------------------------------------------------------------------

disp('Chirp Computation Complete')
toc

%--------------------------------------------------------------------------

% Save arrays.
filename = 'Chirp1.188_' + string(seed) + '.mat';
save(filename,'n','C1','C2','lambda1','lambda2','M1','M2');

%--------------------------------------------------------------------------

end

