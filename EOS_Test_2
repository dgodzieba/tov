%% Random Walk Computation
% n       = 1000; % Number of good EOS we want to generate.
% G       = zeros(n,4); % 2D array, stores adiabatic indices of good EOS.
% lgrho   = zeros(n,3); % Stores lgrho values of good EOS.
% rhoc    = zeros(1,n); % Stores central density of 1.4 solar mass NS.
% rhom    = zeros(1,n); % Stores central density of maximal mass NS.
% Mmax    = zeros(1,n); % Stores the maximum stable mass of each EOS.
% Rmax    = zeros(1,n); % Stores the radius of maximum mass NS.
% cs2     = zeros(1,n); % Stores max cs2 for maximum mass NS.
% lgrhonuc = 14 + log10(2.7); % Nuclear density.
% K0 = 3.59389*1e13;
% lgK0 = log10(K0);
% 
% g0 = [1.356920000000000   4.320057347658784   0.835215074862220   2.399222775840942];
% r0 = [14.324726706844960  14.829004038571814  15.202340810614219];
% 
% G0 = g0(1);
% G1 = g0(2);
% G2 = g0(3);
% G3 = g0(4);
% 
% lgrho0 = r0(1);
% lgrho1 = r0(2);
% lgrho2 = r0(3);
% 
% y  = G0*13 + lgK0;
% y0 = G0*(lgrho0) + lgK0;
% y1 = G1*(lgrho1-lgrho0) + y0;
% y2 = G2*(lgrho2-lgrho1) + y1;
% y3 = G3*(15.5-lgrho2) + y2;
% %plot([0 lgrho0 lgrho1 lgrho2 15.5], [log10(K0) y0 y1 y2 y3], 'k')
% 
% p0 = [lgrho0 y0; lgrho1 y1; lgrho2 y2; 15.5 y3];
% 
% dt = 5e-2;
% for i=1:n
%     while 1
%         a0 = p0(1,:) + dt*[1 G0]*(-1)^randi(2);
%         
%         theta = 2*pi*rand;
%         b0 = p0(2,:) + dt*[cos(theta) sin(theta)];
%         
%         theta = 2*pi*rand;
%         c0 = p0(3,:) + dt*[cos(theta) sin(theta)];
%         
%         d0 = p0(4,:) + dt*[0 1]*(-1)^randi(2);
%         %------------------------------------------------------------------
%         if a0(1)>14.5 || a0(1)<13.6
%             continue
%         end
%         if b0(1)>(lgrhonuc+log10(8)) || b0(1)<(lgrhonuc+log10(1.5))
%             continue
%         end
%         if c0(1)>(lgrhonuc+log10(8.5)) || c0(1)<b0(1)
%             continue
%         end
%         %------------------------------------------------------------------
%         g1 = (b0(2)-a0(2))/(b0(1)-a0(1));
%         if g1>5 || g1<1.4
%             continue
%         end
%         g2 = (c0(2)-b0(2))/(c0(1)-b0(1));
%         if g2>8 || g2<0
%             continue
%         end
%         g3 = (d0(2)-c0(2))/(d0(1)-c0(1));
%         if g3>8 || g3<0.5
%             continue
%         end
%         %------------------------------------------------------------------
%         eos = Create_EOS(g1,g2,g3,a0(1),b0(1),c0(1));
%         tov1 = TOVIterate(eos.rhoc,eos,2,'M',1.4,1.0e-9,200);
%         %------------------------------------------------------------------
%         if size(tov1,1)==0
%             continue
%         end
%         lambda2 = tov1.kl(2)*2/3 / tov1.C^5;
%         if lambda2 > 800
%             continue
%         end
%         %------------------------------------------------------------------
%         tov2 = TOVMax(2*eos.rhonuc,eos,'M');
%         %------------------------------------------------------------------
%         if max(tov2.cs2) >= 1
%             continue
%         end
%         if tov2.M > 3 || tov2.M < 1.97
%             continue
%         end
%         %------------------------------------------------------------------
%         p0(1,:) = a0;
%         p0(2,:) = b0;
%         p0(3,:) = c0;
%         p0(4,:) = d0;
%         break
%         %------------------------------------------------------------------
%     end
%     disp(i)
%     G(i,:) = eos.G;
%     lgrho(i,:) = eos.lgrho;
%     rhoc(i) = tov1.rhoc;
%     rhom(i) = tov2.rhoc;
%     Mmax(i) = tov2.M;
%     Rmax(i) = tov2.R;
%     cs2(i) = max(tov2.cs2);
% end





%% Tidal Deformability Calculations
n2      = 15; % Number of density values for which to solve per EOS.
C       = zeros(n,n2); % 2D array, stores the compactness of each EOS
Love    = zeros(3*n,n2); % 2D array, stores k2, k3, and k4 of each EOS
LoveS   = zeros(3*n,n2); % 2D array, stores h2, h3, and h4 of each EOS

rtov = [1e-6:(20-1e-6)/2000:20];
ptovmin = 0;
% Create array of density values to compute Love numbers.
r0 = linspace(5e-4,5e-3,n2);
for i=1:n
    eos = Create_EOS(G(i,2),G(i,3),G(i,4),lgrho(i,1),lgrho(i,2),lgrho(i,3));
    for j=1:n2
        disp(string(i)+" "+string(j)) % Prints current step.
        % Solve TOV eq. for each density value.
        tov  = TOVL(r0(j),eos,[2,3,4],rtov,ptovmin,0);
        % Skip over any errors.
        if size(tov,1)==0
            continue
        end
        % Store k2, k3, and k4 Love numbers for each mass value.
        Love(3*i-2,j) = tov.kl(2);
        Love(3*i-1,j) = tov.kl(3);
        Love(3*i,j) = tov.kl(4);
        % Store h2, h3, and h4 Love numbers for each mass value.
        LoveS(3*i-2,j) = tov.hl(2);
        LoveS(3*i-1,j) = tov.hl(3);
        LoveS(3*i,j) = tov.hl(4);
        % Store compactness for each mass value.
        C(i,j) = tov.C;
    end
end


Lambdas = zeros(3*n,n2); % 2D array, stores lambda2, lambda3, and lambda4.
Etas = zeros(3*n,n2); % 2D array, stores eta2, eta3, and eta4.
for i=1:n
    for j=1:n2
        % Compute lambdas and etas from Love numbers and compactness.
        Lambdas(3*i-2,j) = Love(3*i-2,j)*2/3 / C(i,j)^5;
        Lambdas(3*i-1,j) = Love(3*i-1,j)*2/15 / C(i,j)^7;
        Lambdas(3*i,j) = Love(3*i,j)*2/105 / C(i,j)^9;
        Etas(3*i-2,j) = LoveS(3*i-2,j)*2/3 / C(i,j)^5;
        Etas(3*i-1,j) = LoveS(3*i-1,j)*2/15 / C(i,j)^7;
        Etas(3*i,j) = LoveS(3*i,j)*2/105 / C(i,j)^9;
    end
end





















