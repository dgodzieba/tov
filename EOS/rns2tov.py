#!/usr/bin/env python

import sys

print("#\n"*3),
sys.stdin.readline()
sys.stdin.readline()
sys.stdin.readline()
nlines = int(sys.stdin.readline())
sys.stdin.readline()
sys.stdin.readline()
sys.stdin.readline()
print(nlines)
print("#\n"*3),

for line in sys.stdin:
    i, nb, rho, p = [float(x) for x in line.split()]
    i = int(i)
    nb *= 10**(39)
    print("{:4d} {:1.7e} {:1.7e} {:1.7e}".format(i, nb, rho, p))
